## Домашнее задание `Stream processing и Идемпотетность и коммутативность API в HTTP и очередях`

### Stream processing

#### ТЕОРЕТИЧЕСКАЯ ЧАСТЬ 

- HTTP взаимодействие

![Untitled Diagram.vpd-HTTP.png](./IDL/1%20-%20HTTP/Untitled Diagram.vpd-HTTP.png)

[billing-service.yaml](./IDL/1%20-%20HTTP/billing-service.yaml)
[notification-service.yaml](./IDL/1%20-%20HTTP/notification-service.yaml)
[order-service.yaml](./IDL/1%20-%20HTTP/order-service.yaml)


- Событийное взаимодействие с использование брокера сообщений для нотификаций (уведомлений)

![Untitled Diagram.vpd-Notificaiton message.png](./IDL/2%20-%20Notification%20event/Untitled%20Diagram.vpd-Notificaiton%20message.png)

[billing-service.yaml](./IDL/2%20-%20Notification%20event/billing-service.yaml)
[notification-service.yaml](./IDL/2%20-%20Notification%20event/notification-service.yaml)
[order-service.yaml](./IDL/2%20-%20Notification%20event/order-service.yaml)

- Event Collaboration (реализовани этот вариант)

![Untitled Diagram.vpd-Event collobaration.png](./IDL/3%20-%20Event%20Collaboration/Untitled%20Diagram.vpd-Event%20collobaration.png)

[billing-service.yaml](./IDL/3%20-%20Event%20Collaboration/billing-service.yaml)
[notification-service.yaml](./IDL/3%20-%20Event%20Collaboration/notification-service.yaml)
[order-service-event.yaml](./IDL/3%20-%20Event%20Collaboration/order-service-event.yaml)
[order-service-http.yaml](./IDL/3%20-%20Event%20Collaboration/order-service-http.yaml)

#### ПРАКТИЧЕСКАЯ ЧАСТЬ

- используется namespace default
- запустить kafka ```helm install cp confluentinc/cp-helm-charts -f cp_values.yaml```
- запустить order-service ```helm install order-service  ./order-service-chart/```
- запустить billing-service ```helm install billing-service  ./billing-service-chart/```
- запустить notification-service ```helm install notification-service  ./notification-service-chart/```
- выполнить проверку сценария ```newman run postman/otus_hmwrk_6.postman_collection.json```

### Идемпотетность и коммутативность API в HTTP и очередях

Используется ключ идемпотентности. При создании заказа заполняется X-Request-Id 
В постмае выполняется 2 запроса
- create succesful order
- create dublicate order

В 'create dublicate order'  ключ не обновляется и запрос возвращает код 422 