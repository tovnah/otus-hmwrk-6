package ru.otus.homework.notificationservice;

import lombok.Data;

@Data
public class Order {
    private Long id;
    private Long userId;
    private String userName;
    private Integer amount;
    private String status;
}
