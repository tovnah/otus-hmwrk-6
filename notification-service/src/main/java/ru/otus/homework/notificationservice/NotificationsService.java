package ru.otus.homework.notificationservice;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.apache.kafka.common.protocol.types.Field;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.security.SecureRandom;

@Service
public class NotificationsService {
    public void failureOrder(String message) {

        saveNotification(message, "Failed to create order");
    }

    public void sucessOrder(String message) {
        saveNotification(message, "Order succesfully created");
    }

    @SneakyThrows
    private void saveNotification(String message, String text) {
        Order order = objectMapper.readValue(message, Order.class);

        Notification notification = new Notification();

        notification.setMessage(text);
        notification.setOrderId(order.getId());
        notification.setUserId(order.getUserId());

        notificationRepository.save(notification);
    }

    @Autowired
    private NotificationRepository notificationRepository;
    @Autowired
    private ObjectMapper objectMapper;
}
