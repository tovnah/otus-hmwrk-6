package ru.otus.homework.notificationservice;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "notification_entity")
public class Notification {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private Long id;
    private Long userId;
    private Long orderId;
    private String message;
}
