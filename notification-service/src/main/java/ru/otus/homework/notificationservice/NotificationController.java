package ru.otus.homework.notificationservice;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("notification")
public class NotificationController {

    @RequestMapping("/{orderId}")
    @SneakyThrows
    public List<Notification> create(@PathVariable(value = "orderId") Long orderId, HttpServletResponse response) {
        List<Notification> entities = notificationRepository.findByOrderId(orderId);

        return entities;
    }

    @Autowired
    private NotificationRepository notificationRepository;
}
