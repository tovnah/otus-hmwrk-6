package ru.otus.homework.notificationservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NitificationServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(NitificationServiceApplication.class, args);
    }

}
