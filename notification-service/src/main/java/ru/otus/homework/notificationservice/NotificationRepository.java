package ru.otus.homework.notificationservice;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface NotificationRepository extends CrudRepository<Notification, Long> {
    List<Notification> findByOrderId(Long orderId);
}
