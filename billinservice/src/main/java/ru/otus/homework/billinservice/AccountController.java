package ru.otus.homework.billinservice;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Optional;

@Slf4j
@RestController
@RequestMapping("account")
public class AccountController {

    @RequestMapping(method = RequestMethod.PUT)
    @SneakyThrows
    public Account addAmount(@RequestBody Payment payment, HttpServletResponse response) {
        log.info("New transaction {}", payment);
        Account account = accountRepository.findByUserId(payment.getUserId());

        account.setAmount(account.getAmount() + payment.getAmount());
        Account savedAccount = accountRepository.save(account);
        log.info("Account after accrual and saving {}", savedAccount);

        return savedAccount;
    }

    @RequestMapping("/{userId}")
    @SneakyThrows
    public Account create(@PathVariable(value = "userId") Long userId, HttpServletResponse response) {
        Account userEntityOptional = accountRepository.findByUserId(userId);
        if (userEntityOptional == null) {
            response.setStatus(404);
            return null;
        }
        return userEntityOptional;
    }

    @GetMapping(path = "all")
    @SneakyThrows
    public Iterable<Account> getAll(HttpServletResponse response) {
        return accountRepository.findAll();
    }

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private AccountRepository accountRepository;

}
