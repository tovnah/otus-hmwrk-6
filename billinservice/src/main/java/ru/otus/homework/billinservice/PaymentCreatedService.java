package ru.otus.homework.billinservice;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class PaymentCreatedService {
    @SneakyThrows
    public void execute(String message) {
        Order order = objectMapper.readValue(message, Order.class);
        Account account = accountRepository.findByUserId(order.getUserId());
        log.info("Founded account {}", account);
        if (account.getAmount() < order.getAmount()) {

            kafkaTemplate.send("orderunsuccess", message);

            log.info("Send notification  bad");
            return;
        }
        account.setAmount(account.getAmount() - order.getAmount());
        Account save = accountRepository.save(account);

        log.info("After payment {}", save);
        log.info("Send notification good");

        kafkaTemplate.send("ordersuccess", message);

    }

    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;
    @Autowired
    private AccountRepository accountRepository;
    @Autowired
    private ObjectMapper objectMapper;
}
