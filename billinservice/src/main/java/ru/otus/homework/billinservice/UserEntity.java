package ru.otus.homework.billinservice;

import lombok.Data;

import javax.persistence.*;

@Data
public class UserEntity {
    private Long id;
    private String name;
}
