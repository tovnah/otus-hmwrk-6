package ru.otus.homework.billinservice;

import org.springframework.data.repository.CrudRepository;

public interface AccountRepository extends CrudRepository<Account, Long> {
    Account findByUserId(Long usertId);
}
