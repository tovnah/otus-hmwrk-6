package ru.otus.homework.billinservice;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class UserCreatedService {
    @SneakyThrows
    public void execute(String message) {
        UserEntity user = objectMapper.readValue(message, UserEntity.class);
        Account newAccount = new Account();
        newAccount.setUserId(user.getId());
        newAccount.setAmount(0);

        Account save = accountRepository.save(newAccount);

        log.info("New account was created {}", save);
        //send account created
    }

    @Autowired
    private AccountRepository accountRepository;
    @Autowired
    private ObjectMapper objectMapper;
}
