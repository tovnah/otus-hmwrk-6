package ru.otus.homework.billinservice;

import lombok.Data;

@Data
public class Payment {
    private Long userId;
    private Integer amount;
}
