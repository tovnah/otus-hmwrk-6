package ru.otus.homework.billinservice;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.kafka.annotation.EnableKafka;

@Slf4j
@EnableKafka
@SpringBootApplication
public class BillinserviceApplication {

    public static void main(String[] args) {
        SpringApplication.run(BillinserviceApplication.class, args);
        log.info("FUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUCK");
    }

}
