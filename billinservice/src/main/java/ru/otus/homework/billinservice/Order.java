package ru.otus.homework.billinservice;

import lombok.Data;

import javax.persistence.*;

@Data
public class Order {
    private Long id;
    private Long userId;
    private String userName;
    private Integer amount;
    private String status;
}
