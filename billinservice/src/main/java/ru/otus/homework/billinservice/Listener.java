package ru.otus.homework.billinservice;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.common.protocol.types.Field;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import java.io.DataInput;

@Slf4j
@Service
public class Listener {
    @SneakyThrows
    @KafkaListener(topics = "paymentcreated")
    public void paymentCreated(String message) {
        log.info("New message for payment.created {}", message);
        paymentCreatedService.execute(message);


    }

    @SneakyThrows
    @KafkaListener(topics = "usercreated")
    public void userCreated(String message) {
        log.info("New message for user.created {}", message);
        userCreatedService.execute(message);


    }

    @Autowired
    private UserCreatedService userCreatedService;

    @Autowired
    private PaymentCreatedService paymentCreatedService;
}
