package ru.otus.homework.orderservice;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Slf4j
@Service
public class OrderService {
    public void actionUnsuccess(String message) {
        changeState(message, "unsuccess");
    }

    public void actionSuccess(String message) {
        changeState(message, "success");
    }

    @SneakyThrows
    private void changeState(String message, String status) {
        Order order = objectMapper.readValue(message, Order.class);
        order.setStatus(status);
        Order save = orderRepository.save(order);

        log.info("Saved order {}", save);
    }

    @Autowired
    private OrderRepository orderRepository;
    @Autowired
    private ObjectMapper objectMapper;
}
