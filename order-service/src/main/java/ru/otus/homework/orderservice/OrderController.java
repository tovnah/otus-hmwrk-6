package ru.otus.homework.orderservice;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("order")
public class OrderController {

    @PostMapping()
    @SneakyThrows
    public Order create(@RequestBody Order order, HttpServletResponse response, @RequestHeader Map<String, String> headers) {

        List<Order> orders = orderRepository.findByClientId(headers.get("x-request-id"));
        if (!CollectionUtils.isEmpty(orders)) {
            response.setStatus(422);
            return null;
        }
        order.setClientId(headers.get("x-request-id"));
        order.setStatus("created");
        Order savedOrder = orderRepository.save(order);

        kafkaTemplate.send("paymentcreated", objectMapper.writeValueAsString(savedOrder));

        return savedOrder;
    }

    @RequestMapping("/{orderId}")
    @SneakyThrows
    public Order create(@PathVariable(value = "orderId") Long orderId, HttpServletResponse response) {
        Optional<Order> userEntityOptional = orderRepository.findById(orderId);
        if (!userEntityOptional.isPresent()) {
            response.setStatus(404);
            return null;
        }
        return userEntityOptional.get();
    }


    @GetMapping(path = "all")
    @SneakyThrows
    public Iterable<Order> create() {
        return orderRepository.findAll();
    }

    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;
    @Autowired
    private OrderRepository orderRepository;
}
