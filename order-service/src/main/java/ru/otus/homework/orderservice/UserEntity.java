package ru.otus.homework.orderservice;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "user_entity")
public class UserEntity {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private Long id;

    private String name;
}
