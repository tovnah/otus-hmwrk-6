package ru.otus.homework.orderservice;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "order_entity")
public class Order {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private Long id;
    private Long userId;
    private String userName;
    private Integer amount;
    private String status;

    private String clientId;
}
