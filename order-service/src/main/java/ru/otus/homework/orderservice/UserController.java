package ru.otus.homework.orderservice;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.Optional;

@RestController
@RequestMapping("user")
public class UserController {

    @PostMapping()
    @SneakyThrows
    public UserEntity create(@RequestBody UserEntity userEntity, HttpServletResponse response) {
        UserEntity saveduser = userRepository.save(userEntity);
        kafkaTemplate.send("usercreated", objectMapper.writeValueAsString(saveduser));
        return saveduser;
    }

    @RequestMapping("/{userId}")
    @SneakyThrows
    public UserEntity create(@PathVariable(value="userId")Long userId, HttpServletResponse response) {
        Optional<UserEntity> userEntityOptional = userRepository.findById(userId);
        if (!userEntityOptional.isPresent()) {
            response.setStatus(404);
            return null;
        }
        return userEntityOptional.get();
    }

    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;
    @Autowired
    private UserRepository userRepository;
}
