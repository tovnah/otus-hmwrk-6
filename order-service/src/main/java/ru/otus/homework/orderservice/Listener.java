package ru.otus.homework.orderservice;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class Listener {
    @SneakyThrows
    @KafkaListener(topics = "orderunsuccess")
    public void paymentCreated(String message) {
        log.info("New message for orderunsuccess {}", message);
        orderService.actionUnsuccess(message);


    }

    @SneakyThrows
    @KafkaListener(topics = "ordersuccess")
    public void userCreated(String message) {
        log.info("New message for orderunsuccess {}", message);
        orderService.actionSuccess(message);
    }

    @Autowired
    private OrderService orderService;
}
