package ru.otus.homework.orderservice;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "account_entity")
public class Account {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private Long id;
    private Long userId;
    private Integer amount;
}
